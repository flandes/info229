# info229, Mathématiques pour l'informatique

Ceci est la page web (gitlab) du cours de L2 informatique info229, Mathématiques pour l'informatique

Pour télécharger ce repo (repository, dossier distant), faites:
`git clone git@gitlab.inria.fr:flandes/info229.git`

Pour le mettre à jour (car des corrigés ou autres fichiers seront progressivement ajoutés/modifiés)
`git pull`
depuis le dossier où vous avez cloné. (Ainsi, vous ne téléchargez que les différences, c'est plus écolo).

Si vous n'arrivez pas avec `git`, vous pouvez aussi juste télécharger tout (bouton download à coté du bouton bleu "clone" sur l'interface) ou fichier par fichier.

Licence du poly de **cours**: [CC-BY-SA] https://fr.wikipedia.org/wiki/Licence_Creative_Commons
Les **énoncés de TD** n'étants pas tous de moi, et ne connaissant pas exactement la source, ils sont "tous droits réservés".

# Actualités

Les DM sont à faire en BINOMES ! Donc, pénalité de -1 (note sur 10) pour rendu en solo
Rappel: un non rendu vaut 0. Pas de triche ! Attention à la copie entre groupes: surtout quand c'est scandaleusement faux !!

Bases de l'algèbre:
les poly de cours de la L1 d'Orsay sont ici:
- Semestre 1: https://www.imo.universite-paris-saclay.fr/~riou/enseignement/2020-2021/l1-algebre/cours-portable.pdf
- Semestre 2: http://valentinhernandez.perso.math.cnrs.fr/meu2021.html

# Astuces diverses:

- pour des **difficultés sociales et matérielles** liées à l’équipement ou aux conditions de vie :
    - Contact : Pôle Vie Pratique, Cellule Aide Sociale, aides.etudiant@universite-paris-saclay.fr
    - Contact social Crous : service-social.paris-saclay@crous-versailles.fr

L'université a lancé un site où sont recensées toutes les aides auxquelles vous pouvez prétendre (internes et externes). Il vous suffit de créer un compte ici :
https://app.toutesmesaides.fr/bienvenue/v1/company/upsaclay/334aef2a-74d4-4d44-8cfd-959fac5b6710

**Astuces:** (pour rendus de DMs par email)
- compression d'images : quand vous prenez votre copie en HD avec un appareil à 20 Mégapixels, c'est horrible (pour moi, pour la planète, pour tout le monde). Sous linux, vous avez `mogrify -quality 70% *.jpg` qui convertit tous les .jpg du répertoire courant en abaissant leur qualité (invisible à l'oeil nu), et leur taille (énormément). Attention, c'est une opération *in-place* (pas en copie). Vous perdez les originaux.
- vous avez aussi https://www.camscanner.com/ , qui est plutôt mieux que la prise de photo à la main (si vous n'avez pas de scanner). Si vous avez une alternative libre, signalez-la moi.
- fabriquer un seul pdf à partir de trucs:
    - commencer par donner des noms qui respectent l'ordre alphanumérique, par exemple 1.jpg, 2.jpg, etc pour vos scans
    - utilisez ensuite `pdfjoin *.jpg`, ou qqchose dans le genre, avec une regexp qui vise vos fichiers sources. Ça marche aussi pour les `.png`, les `.pdf`, etc. Vive linux.


